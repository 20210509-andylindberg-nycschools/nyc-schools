//
//  SchoolModel.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import Foundation

class SchoolModel: Decodable {

    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overview = "overview_paragraph"
        case neighborhood
        case location
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case totalStudents = "total_students"
        case city
        case zip
        case state = "state_code"
        case latitude
        case longitude
    }
        
    let dbn: String?
    let schoolName: String?
    let boro: String?
    let overview: String?
    let neighborhood: String?
    let location: String?
    let phoneNumber: String?
    let schoolEmail: String?
    let website: String?
    let totalStudents: String?
    let city: String?
    let zip: String?
    let state: String?
    let latitude: String?
    let longitude: String?
    
    var satCriticalReadingAvgScore: String? = nil
    var satMathAvgScore: String? = nil
    var satWritingAvgScore: String? = nil

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        dbn = try? container.decodeIfPresent(String.self, forKey: .dbn)
        schoolName = try? container.decodeIfPresent(String.self, forKey: .schoolName)
        boro = try? container.decodeIfPresent(String.self, forKey: .boro)
        overview = try? container.decodeIfPresent(String.self, forKey: .overview)
        neighborhood = try? container.decodeIfPresent(String.self, forKey: .neighborhood)
        location = try? container.decodeIfPresent(String.self, forKey: .location)
        phoneNumber = try? container.decodeIfPresent(String.self, forKey: .phoneNumber)
        schoolEmail = try? container.decodeIfPresent(String.self, forKey: .schoolEmail)
        website = try? container.decodeIfPresent(String.self, forKey: .website)
        totalStudents = try? container.decodeIfPresent(String.self, forKey: .totalStudents)
        city = try? container.decodeIfPresent(String.self, forKey: .city)
        zip = try? container.decodeIfPresent(String.self, forKey: .zip)
        state = try? container.decodeIfPresent(String.self, forKey: .state)
        latitude = try? container.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try? container.decodeIfPresent(String.self, forKey: .longitude)
    }
}
