//
//  ViewController.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import UIKit

protocol NYCSchoolsViewDelegate: AnyObject {
    func refreshView()
}

class NYCSchoolsViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var viewModel = NYCSchoolsViewModel(networkClient: NetworkClient())

    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }

    private var searchBarIsEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDelegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        setupSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchSchoolInfo()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshView()
    }

    func setupSearchController(){
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Schools"
        searchController.searchBar.tintColor = UIColor.white
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        viewModel.filterContent(on: searchText)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Pass the selected school with sat score to the destinatiion view controller
        if segue.identifier == StringConstants.schoolSATScoreSegue{
            let schoolWithSATScoreVC = segue.destination as! SchoolDetailsTableViewController
            if let schoolWithSATScore = sender as? SchoolModel {
                schoolWithSATScoreVC.school = schoolWithSATScore
            }
        }
    }
}

extension NYCSchoolsViewController: NYCSchoolsViewDelegate {
    func refreshView() {
        tableView.reloadData()
    }
}

extension NYCSchoolsViewController {
    private func getSchoolModel(for indexPath: IndexPath) -> SchoolModel {
        return isFiltering ? viewModel.searchFilteredSchoolArray[indexPath.row] : viewModel.schoolArray[indexPath.row]
    }
}

extension NYCSchoolsViewController: UITableViewDataSource, UITableViewDelegate {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return viewModel.searchFilteredSchoolArray.count
        }
        
        return viewModel.schoolArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SchoolTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: StringConstants.schoolCellIdentifier, for: indexPath) as! SchoolTableViewCell
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.borderColor = UIColor.white.cgColor
        let schoolList = getSchoolModel(for: indexPath)

        if let schoolName = schoolList.schoolName {
            cell.schoolNameLabel.text = schoolName
        }
        
        if let schoolAddress = schoolList.location {
            let address = JSONUtils.getTrimmedSchoolAddress(schoolAddress)
            cell.schoolAddressLabel.text = "Address: \(address)"
        }
        
        if let phoneNumber = schoolList.phoneNumber{
            cell.schoolPhoneNumberLabel.text = phoneNumber
        }

        cell.layoutIfNeeded()

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let schools = getSchoolModel(for: indexPath)
        let selectedSchool = schools
        self.performSegue(withIdentifier: StringConstants.schoolSATScoreSegue, sender: selectedSchool)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension NYCSchoolsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
