//
//  SchoolDetailsViewController.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import UIKit

class SchoolDetailsTableViewController: UITableViewController {
    
    var school: SchoolModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = school.schoolName
        self.tableView.rowHeight = UITableView.automaticDimension
    }
}


extension SchoolDetailsTableViewController {
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return SchoolDetailCellFactory.tableViewCellWithSATScore(self.tableView, school: self.school)
        case 1:
            return SchoolDetailCellFactory.tableViewCellWithOverView(self.tableView, school: self.school)
        case 2:
            return SchoolDetailCellFactory.tableViewCellWithContactInfo(self.tableView, school: self.school)
        default:
            return SchoolDetailCellFactory.tableViewCellWithMapView(self.tableView, school: self.school)
        }
    }
    
//Return a square for the cell with map
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0,1,2:
            return UITableView.automaticDimension
        default:
            return UIScreen.main.bounds.width
        }
    }
    
}
