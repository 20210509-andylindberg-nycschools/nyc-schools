//
//  NYCSchoolsViewModel.swift
//  20210509-AndyLindberg-NYCSchools
//

import Foundation

class NYCSchoolsViewModel {

    weak var viewDelegate: NYCSchoolsViewDelegate?
    let networkClient: NetworkClientProvider

    var schoolArray: [SchoolModel] = []
    var searchFilteredSchoolArray = [SchoolModel]()

    init(networkClient: NetworkClientProvider) {
        self.networkClient = networkClient
    }

    func fetchSchoolInfo() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.fetchSchoolInformation()
        }
    }

    func filterContent(on searchText: String) {
        searchFilteredSchoolArray = (schoolArray.filter({( schools : SchoolModel) -> Bool in
            return schools.schoolName!.lowercased().contains(searchText.lowercased())
        }))

        viewDelegate?.refreshView()
    }
}

extension NYCSchoolsViewModel {
    private func fetchSchoolInformation(){
        networkClient.fetchSchools { [weak self] schoolArray in
            self?.schoolArray = schoolArray
            self?.fetchSATInformation()
        }
    }

    private func fetchSATInformation(){
        networkClient.fetchSAT { [weak self] result in
            if let result = result {
                self?.addSATScoreToSchool(result)
                DispatchQueue.main.async {[weak self] in
                    self?.viewDelegate?.refreshView()
                }
            }
        }
    }

    private func addSATScoreToSchool(_ satScoreObject: Any){
        guard let schoolsWithSatScoreArray = satScoreObject as? [[String: Any]] else{
            return
        }

        for  schoolsWithSatScore in schoolsWithSatScoreArray{
            if let matchedDBN = schoolsWithSatScore["dbn"] as? String{
                //Use the dbn parameter to find the correct school to set the three SAT values for
                let matchedSchool = self.schoolArray.first(where: { (school) -> Bool in
                    return school.dbn == matchedDBN
                })

                guard matchedSchool != nil else{
                    continue
                }

                if let satReadingScoreObject =  schoolsWithSatScore["sat_critical_reading_avg_score"] as? String{
                    matchedSchool!.satCriticalReadingAvgScore = satReadingScoreObject
                }

                if let satMathScoreObject = schoolsWithSatScore["sat_math_avg_score"] as? String{
                    matchedSchool!.satMathAvgScore = satMathScoreObject
                }

                if let satWritingScoreObject =  schoolsWithSatScore["sat_writing_avg_score"] as? String{
                    matchedSchool!.satWritingAvgScore = satWritingScoreObject
                }

            }
        }
    }
}
