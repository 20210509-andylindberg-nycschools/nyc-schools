//
//  SchoolTableViewCell.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {  
    @IBOutlet var schoolNameLabel: UILabel!
    @IBOutlet var schoolAddressLabel: UILabel!
    @IBOutlet var schoolPhoneNumberLabel: UILabel!
}
