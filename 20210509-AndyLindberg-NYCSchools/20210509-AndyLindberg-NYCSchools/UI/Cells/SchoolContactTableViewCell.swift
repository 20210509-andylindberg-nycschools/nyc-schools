//
//  SchoolContactTableViewCell.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import UIKit

class SchoolContactTableViewCell: UITableViewCell {

    @IBOutlet var schoolAddressLabel: UILabel!
    @IBOutlet var schoolPhoneNumberLabel: UILabel!
    @IBOutlet var schoolWebsiteLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }


}

