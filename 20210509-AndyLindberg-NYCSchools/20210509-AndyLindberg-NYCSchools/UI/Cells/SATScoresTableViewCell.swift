//
//  SATScoresTableViewCell.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import UIKit

class SATScoresTableViewCell: UITableViewCell {

    @IBOutlet var schoolNameLabel: UILabel!
    @IBOutlet var schoolSATReadingScoreLabel: UILabel!
    @IBOutlet var schoolSATMathScoreLabel: UILabel!
    @IBOutlet var schoolSATWritingScoreLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }


}
