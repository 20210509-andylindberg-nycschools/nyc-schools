//
//  SchoolDetailCellFactory.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import Foundation
import UIKit
import MapKit

class SchoolDetailCellFactory {
    // Return a UITableViewCell using a SchoolModel with SAT Score
    static func tableViewCellWithSATScore(_ tableView: UITableView, school: SchoolModel) -> UITableViewCell{
        let schoolWithSATScoresCell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailConstants.Cells.schoolWithSATScoreCellIdentifier) as! SATScoresTableViewCell
        
        schoolWithSATScoresCell.schoolNameLabel.text = school.schoolName
        
        //If there is no SAT data for the school, let the user know that it is not available
        schoolWithSATScoresCell.schoolSATReadingScoreLabel.text = (school.satCriticalReadingAvgScore != nil) ?  (SchoolDetailConstants.averageSATReadingScore + school.satCriticalReadingAvgScore!) : SchoolDetailConstants.noSATScoreInfomationText
        
        // Set the math average score
        schoolWithSATScoresCell.schoolSATMathScoreLabel.isHidden = (school.satMathAvgScore != nil) ? false : true
        schoolWithSATScoresCell.schoolSATMathScoreLabel.text = (school.satMathAvgScore != nil) ? (SchoolDetailConstants.averageSATMathScore + school.satMathAvgScore!) : nil
        
        // Set the writing average score
        schoolWithSATScoresCell.schoolSATWritingScoreLabel.isHidden =  (school.satWritingAvgScore != nil) ? false : true
        schoolWithSATScoresCell.schoolSATWritingScoreLabel.text = (school.satWritingAvgScore != nil) ? (SchoolDetailConstants.averageSATWritingScore + school.satWritingAvgScore!) : nil
        
        return schoolWithSATScoresCell
    }

    // Return a UITableViewCell using a SchoolModel with overview description
    static func tableViewCellWithOverView(_ tableView: UITableView, school: SchoolModel) -> UITableViewCell{
        let schoolWithOverviewCell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailConstants.Cells.schoolOverviewCellIdentifier) as! SchoolOverviewTableViewCell
        
        schoolWithOverviewCell.schoolOverviewLabel.text = school.overview
        
        return schoolWithOverviewCell
    }
    
    // Return a UITableViewCell using a SchoolModel with address, phone number, and contact website
    static func tableViewCellWithContactInfo(_ tableView: UITableView, school: SchoolModel) -> UITableViewCell{
        let schoolWithContactCell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailConstants.Cells.schoolWithContactCellIdentifier) as! SchoolContactTableViewCell
        
        schoolWithContactCell.schoolAddressLabel.text = "Address: " + JSONUtils.getTrimmedSchoolAddress(school.location)
        schoolWithContactCell.schoolPhoneNumberLabel.text = (school.phoneNumber != nil) ? "Phone Number: " + school.phoneNumber! : ""
        schoolWithContactCell.schoolWebsiteLabel.text = "Website: " + school.website!
        
        return schoolWithContactCell
    }
    
    // Return a UITableViewCell using a SchoolModel with coordinates. This cell has a small map view with a pin at the coordinates provided
    static func tableViewCellWithMapView(_ tableView: UITableView, school: SchoolModel) -> UITableViewCell{
        let schoolWithAddressCell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailConstants.Cells.schoolWithAddressCellIdentifier) as! SchoolMapTableViewCell
        let latitude = Double(school.latitude ?? "0.0")
        let longitude = Double(school.longitude ?? "0.0")
        let coord = CLLocationCoordinate2D (latitude: latitude!, longitude: longitude!)
        
        schoolWithAddressCell.addAnnotaionWithCoordinates(coord)

        
        return schoolWithAddressCell
    }
}

