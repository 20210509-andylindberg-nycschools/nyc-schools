//
//  SchoolOverviewTableViewCell.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import UIKit

class SchoolOverviewTableViewCell: UITableViewCell {

    @IBOutlet var schoolOverviewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }


}
