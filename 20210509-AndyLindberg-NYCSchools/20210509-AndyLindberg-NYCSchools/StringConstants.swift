//
//  StringConstants.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import Foundation

struct StringConstants {
    static let schoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let satURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    static let schoolCellIdentifier = "schoolCell"
    static let schoolSATScoreSegue = "schoolSATScoreSegue"
}

struct SchoolDetailConstants {
    struct Cells {
        static let schoolWithSATScoreCellIdentifier =  "SATScoresTableViewCell"
        static let schoolOverviewCellIdentifier = "SchoolOverViewTableViewCell"
        static let schoolWithAddressCellIdentifier = "SchoolMapTableViewCell"
        static let schoolWithContactCellIdentifier = "SchoolContactTableViewCell"
    }

    
    static let noSATScoreInfomationText = "No SAT information found for this school"
    static let averageSATReadingScore = "SAT Average Critical Reading Score:  "
    static let averageSATMathScore = "SAT Average Math Score:  "
    static let averageSATWritingScore = "SAT Average Writing Score:  "
}
