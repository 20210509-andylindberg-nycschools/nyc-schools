//
//  NetworkClient.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/10/21.
//

import Foundation

protocol NetworkClientProvider {
    func fetchSchools(onComplete: @escaping (_ result: [SchoolModel]) -> Void)
    func fetchSAT(onComplete: @escaping (_ result: Any?) -> Void)
}

class NetworkClient: NetworkClientProvider {

    func fetchSchools(onComplete: @escaping (_ result: [SchoolModel]) -> Void) {
        guard let schoolsURL = URL(string: StringConstants.schoolURL) else {
            onComplete([])
            return
        }

        let request = URLRequest(url:schoolsURL)
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (schoolData, response, error)  in
            if let schoolData = schoolData {
                do {
                    let decoder = JSONDecoder()
                    let schoolArray = try decoder.decode([SchoolModel].self, from: schoolData)
                    onComplete(schoolArray)

                } catch {
                    print("Error parsing school JSON: \(error.localizedDescription)")
                    onComplete([])
                }
            } else {
                print("Unable to parse JSON due to invalid data")
                onComplete([])
            }
        }
        task.resume()
    }

    func fetchSAT(onComplete: @escaping (_ result: Any?) -> Void) {
        guard let schoolsSATScoreURL = URL(string: StringConstants.satURL) else {
            onComplete(nil)
            return
        }

        let requestForSATScore = URLRequest(url:schoolsSATScoreURL)
        let session = URLSession.shared
        let task = session.dataTask(with: requestForSATScore) { (schoolsWithSATScoreData, response, error) in
            if let schoolsWithSATScoreData = schoolsWithSATScoreData {
                do{
                    let satScoreObject = try JSONSerialization.jsonObject(with: schoolsWithSATScoreData, options: [])
                    onComplete(satScoreObject)
                }catch{
                    print("Error parsing school SAT JSON: \(error.localizedDescription)")
                    onComplete(nil)
                }
            } else {
                print("Unable to parse SAT JSON due to invalid data")
                onComplete(nil)
            }
        }
        task.resume()
    }
}
