//
//  JSONUtils.swift
//  20210509-AndyLindberg-NYCSchools
//
//  Created by Andy Lindberg on 5/9/21.
//

import Foundation

class JSONUtils {
    
    // Since the address we get from the JSON includes the coordinates, trim those off for better appearance.
    static func getTrimmedSchoolAddress(_ schoolAddr: String?) -> String{
        if let schoolAddress = schoolAddr{
            return schoolAddress.components(separatedBy: "(")[0]
        }
        return ""
    }
    
}
