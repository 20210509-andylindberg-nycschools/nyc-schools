# NYC-Schools

NYC Schools

For this sample project I decided to use a separate class to handle the downloading of the school information JSON. From there, this data is passed off to the NYCSchoolsViewModel which is responsible for getting the data we need to display in the view controllers. 

There are no external libraries for this project so no need for Cocoapods. 

Given more time I would have liked to show more of the data available for each school (hence the extra CodingKeys).

I think it would be interesting to sort the schools based on SAT score and see if there are any geographic groupings based on high or low test scores. This could have been done on a separate map from the small one in the SchoolDetailsTableViewController. 
